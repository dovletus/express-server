const { Post } = require("./post");
const { User } = require("./user");

User.hasMany(Post, {
    foreignKey: 'userId'
  })

Post.belongsTo(User)

module.exports = {
    User,
    Post
}