const sequelize = require('../database/db')
const {DataTypes} = require('sequelize')

const Post = sequelize.define("post",{
    id:{
        type: DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement: true,
        allowNull: false
    },
    userId:{
        type: DataTypes.INTEGER,        
        allowNull: false
    },
    title: {
        type:DataTypes.STRING,
        defaultValue:'',
        validate:{
            max:50,
            // customValidator(value) {
            //     if (value > 5) {
            //       throw new Error("Title length must be at max 5 characters");
            //     }
            //   }
        }        
    },
    description : {
        type:DataTypes.STRING,
        defaultValue:'',  
        validate:{
            max:100,            
        }        
    },
    topic: {
        type:DataTypes.STRING,
        defaultValue:'',
        validate:{
            max:50,            
        }  
    },
    seen : {
        type: DataTypes.INTEGER,
        defaultValue:0
    },
    // created_at : {
    //     type: DataTypes.DATE,
    //     defaultValue: DataTypes.NOW,
    //     allowNull:false
    // },
    // updated_at : {
    //     type: DataTypes.DATE,        
    //     allowNull:true
    // }
})

//relationship, association
// Posts.belongsTo(Users)

module.exports = {
    Post
}