const Router = require('express')

const CategoryRouter = require('./categoryRouter')
const ProductRouter = require('./productRouter')
const SignInRouter = require('./signInRouter')
const PostRouter = require('./postRouter')
const UserRouter = require('./userRouter')
const RawQueryRouter =require('./rawQueryRouter')

const router = new Router()


router.use('/categories', CategoryRouter)
router.use('/products', ProductRouter)
router.use('/auth', SignInRouter)
router.use('/posts', PostRouter)
router.use('/users', UserRouter)
router.use('/raw-query', RawQueryRouter)



module.exports = router