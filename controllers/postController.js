const {v4 : uuidv4} = require('uuid')

const {Post, User} = require('../models')
const {Op} = require('sequelize')

class PostController {

    async getPosts(req, res, next){
        // const posts = await Post.findAll({include: User}) // no need user in list
        const posts = await Post.findAndCountAll()        
        res.json(posts)

    }

    async getPostById(req, res, next){
        // const id = req.params.id
        const {id, title, description, seen, author, operation} = req.body
        // const category = await Post.findOne({where:{id:id}})

        const criteria = []
        if(id) criteria.push({id:id})
        if(title) criteria.push({title:title})
        if(description) criteria.push({description:description})
        if(seen) criteria.push({seen:seen})
        if(author) criteria.push({author:author})



        const category = await Post.findOne({
            where:{
                [Op[operation]]: [
                    //{id},{title},{description},{seen}
                    ...criteria
                ]
            },
            include: User    
            // include: {model: User}
        })

        res.json(category)
    }

    async addPost(req, res, next){
        const {userId, title, description, author, seen} = req.body
        if(!userId) return res.json({success:false, message: 'userId must not be 0'})

        const user = await Post.create( {userId, title, description, author, seen});
        // console.log("user", user)
        res.json({success: true, newUser: user})
    }

    async updatePost(req,res, next){
        const post = req.body
        const result = await Post.update(post, {where:{id:post.id}})
        res.json({result:result[0]})
    }

    async deletePost(req, res, next){
        const id = req.params.id        
        
        const result = await Post.destroy({where: {id}})
         res.json({result} )

    }
}

module.exports = new PostController()