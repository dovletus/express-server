const {v4 : uuidv4} = require('uuid')

const db = require('../db.json')

class CategoryController {

    getList(req, res, next){
        // res.send('Category list')
        res.json(db.categories)

    }

    getCategoryById(req, res, next){
        const id = req.params.id        
        const category = db.categories.find(cat=>cat.id==id)

        res.json(category)
    }

    addCategory(req, res, next){
        const user = req.user
        console.log('user is', user)

        const newCategory = req.body
        newCategory.id = uuidv4()
        // console.log('new category is ', newCategory)
        res.json(newCategory)
    }

    updateCategory(req,res, next){
        const user = req.user
        console.log('user is', user)
        
        const category = req.body
        const newList = db.categories.map(cat=>cat.id==category.id ? category : cat)
        const updatedCategory = newList.find(cat=>cat.id ==category.id)
        res.json(updatedCategory)
    }

    deleteCategory(req, res, next){
        const id = req.params.id        
        const newList = db.categories.filter(cat=>cat.id != id)
        const deletedCategory = newList.find(cat=>cat.id == id)
        res.send(deletedCategory ? false : true)
        // res.json(deletedCategory )

    }
}

module.exports = new CategoryController()