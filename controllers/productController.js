const {v4 : uuidv4} = require('uuid')

class ProductController {

    getList(req, res, next){
        res.send('product list')
    }

    getProductById(req, res, next){
        res.send ('product data with id')
    }

    addProduct(req, res, next){
        res.send('new product with id ' + uuidv4())
    }

    updateProduct(req,res, next){
        res.send ('update product with id')
    }

    deleteProduct(req, res, next){
        res.send ('delete product with id')
    }
}

module.exports = new ProductController()