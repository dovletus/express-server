const SignInController = require('./signInController')
const CategoryController = require('./categoryController')
const ProductController = require('./productController')
const PostController=require('./postController')
const UserController=require('./userController')
const RawQueryController = require('./rawQueryController')



module.exports = {  SignInController,
                    CategoryController,
                    ProductController,  
                    PostController,
                    UserController,
                    RawQueryController
                    }