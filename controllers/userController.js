const {v4 : uuidv4} = require('uuid')


const {Op} = require('sequelize')
const { User, Post } = require('../models')

class UserController {

    async list(req, res, next){
        const users = await User.findAndCountAll()
        // console.log('posts',posts)
        res.json(users)

    }

    async getUserById(req, res, next){
        const {id, name, login} = req.query;

        const criteria = []
        id && criteria.push({id:id})
        name && criteria.push({name:name})
        login && criteria.push({login:login})
        
        // if(id) criteria.push({id:id})
        // if(name) criteria.push({name:name})
        // if(login) criteria.push({login:login})

        // const category = await User.findOne({where:{id}, 
        //     include: Post } )

        const category = await User.findOne({where:
            {
                [Op.and]:[...criteria]
            }, 
            include: Post } )

        res.json(category)
    }

    async addUser(req, res, next){
        const {id, name, login, password, email, tel, web} = req.body
        const user = await User.create( {id, name, login, password, email, tel, web});
        // console.log("user", user)
        res.json({success: true, newUser: user})
    }

    async updateUser(req,res, next){
        const user = req.body
        const result = await User.update(user, {where:{id:user.id}})
        res.json({result:result[0]})
    }

    async deleteUser(req, res, next){
        const id = req.params.id        
        
        const result = await User.destroy({where: {id}})
         res.json({result} )

    }
}

module.exports = new UserController()